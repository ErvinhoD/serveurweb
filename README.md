## Pour qui : 

    Pour les utilisateurs, pour le client qui crée un site web à l'aide d'un developpeur, pour rendre son site accessible sur internet. 

## Expression du besoin : 

    Création d'un serveur web + Mise à jour automatique de tous les paquets système (php, Apache...) 

## Nom du produit et catégorie : 

    Serveur Apache / Plateforme hébergeant une page html, fichiers jpeg...

## Avantages clés :

    - Permettre à l'hebergeur, au client de proposer du contenu sur le net
    - Le client se charge lui même de créer un site qui sera par la suite hébergé sur le serveur
    
## Alternatives : 

    Applications mobiles, serveur IIS (Windows)
    
## Différenciation : 

    Mise à jour automatique des paquets, système... 